#include <ESP8266WiFi.h>

const int GPIO_PIN = 5;

WiFiServer server(80);

void setup() {

  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println();

  // static IP config. change according to your LAN settings
  IPAddress staticIP(192, 168, 1, 53);
  IPAddress gateway(192, 168, 1, 1);
  IPAddress subnet(255, 255, 255, 0);
  IPAddress dns(192, 168, 1, 1);
  WiFi.disconnect();
  WiFi.config(staticIP, gateway, subnet, dns, dns);
  // change wifi name and password and load to board.
  // then comment out this line and leave only WiFi.begin(); and load to board. ESP32 remember WiFi config
  WiFi.begin("WIFI-SSID-NAME", "WIFI-PASSWORD");
  WiFi.begin();
  
  if (WiFi.getMode() != WIFI_STA)
  {
    WiFi.mode(WIFI_STA);
    WiFi.begin();
    Serial.println("WiFi begin");
  }

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());

  server.begin();
}

// prepare a web page to be send to a client (web browser)
String prepareHtmlPage()
{
  String htmlPage;
  htmlPage.reserve(1024);               // prevent ram fragmentation
  htmlPage = F("HTTP/1.1 200 OK\r\n"
               "Content-Type: text/html\r\n"
               "Connection: close\r\n"  // the connection will be closed after completion of the response
               "\r\n"
               "<!DOCTYPE HTML>"
               "<html>"
               "Hello");
  htmlPage += F("</html>"
                "\r\n");
  return htmlPage;
}

void loop() {


  WiFiClient client = server.available();
  if (client)
  {
    Serial.println("\n[Client connected]");
    while (client.connected())
    {
      // read line by line what the client (web browser) is requesting
      if (client.available())
      {
        String line = client.readStringUntil('\r');
        Serial.print(line);
        int idx;
        if((idx = line.indexOf("/tone")) > 0)
        {
          int freq = atoi(line.substring(idx+5).c_str());
          Serial.printf("Freq: %d\n", freq);
          freq = freq < 100 ? 1000 : freq;
          pinMode(GPIO_PIN, OUTPUT);
          tone(GPIO_PIN, freq);
        }
        else if(line.indexOf("/silent") > 0)
        {
          noTone(GPIO_PIN);
          digitalWrite(GPIO_PIN, LOW);
        }
        // wait for end of client's request, that is marked with an empty line
        if (line.length() == 1 && line[0] == '\n')
        {
          client.println(prepareHtmlPage());
          break;
        }
      }
    }

    while (client.available()) {
      client.read();
    }

    // close the connection:
    client.stop();
    Serial.println("[Client disconnected]");
  }
  delay(200);
}

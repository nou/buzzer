# ESP32 buzzer

Simple ESP32 buzzer that is controlled by http requests

Before you load to boad you need set IP address and WiFi password
So edit lines according to your WiFi settings

  IPAddress staticIP(192, 168, 1, 53);
  IPAddress gateway(192, 168, 1, 1);
  IPAddress subnet(255, 255, 255, 0);
  WiFi.begin("WIFI-SSID-NAME", "WIFI-PASSWORD");  
  
Load this program to board. You can then test it by running `curl http://192.168.1.53/tone`

To silence run `curl http://192.168.1.53/silent`

You may change frequency by appending number after /tone. To get 3kHz tone for example `curl http://192.168.1.53/tone3000`

After everything works comment out `WiFi.begin("WIFI-SSID-NAME", "WIFI-PASSWORD");` line and leave only 
`WiFi.begin();` to get better startup time.
